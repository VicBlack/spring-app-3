package ru.kupriyanov.springcourse;

public class RockMusic implements IMusic {

    @Override
    public String getMusic() {
        return "rock song";
    }

}
