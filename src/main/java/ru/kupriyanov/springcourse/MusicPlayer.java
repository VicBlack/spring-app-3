package ru.kupriyanov.springcourse;

public class MusicPlayer {

    private final IMusic music;

    // IoC
    public MusicPlayer(IMusic music) {
        this.music = music;
    }

    public void playMusic() {
        System.out.println("Playing: " + music.getMusic());
    }

}
